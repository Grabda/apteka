document.addEventListener('DOMContentLoaded', function () {

    function MenuCutter(parent, list) {
        this.parent = parent;
        this.list = list;
        this.ul = null;
    }

    MenuCutter.prototype.sumChildrenWidth = function () {
        let list = this.list;
        let childrenWidth = 0;

        for (let i = 0; i < list.children.length; ++i) {
            childrenWidth += list.children[i].offsetWidth;
        }

        return childrenWidth;
    };

    MenuCutter.prototype.handleElementsAttributes = function () {
        let parentWidth = this.parent.offsetWidth;
        let childrenWidth = this.sumChildrenWidth();
        let li = document.createElement("li");
        this.ul = document.createElement("ul");
        let a = document.createElement("a");

        if (parentWidth <= childrenWidth && screen.width > 767) {
            this.ul.classList.add("nested-list-nav");
            this.ul.classList.add("hide");
            a.classList.add('more');
            a.classList.add('menu-uppercase');
            a.innerHTML = "Więcej";
            li.appendChild(a);
            li.appendChild(this.ul);
            li.classList.add("special-li");
            this.list.appendChild(li);
        }
    };

    MenuCutter.prototype.setScreenSpecialValue = function () {
        this.screenSpecialValue = 1;
        let parentWidth = this.parent.offsetWidth;

        if (parentWidth < 1103 && parentWidth > 753) {
            this.screenSpecialValue = 400;
        } else if (parentWidth > 1102 && parentWidth < 1320) {
            this.screenSpecialValue = 400;
        } else if (parentWidth > 1319 && parentWidth < 1585) {
            this.screenSpecialValue = 500;
        } else if (parentWidth > 1584 && parentWidth < 1847) {
            this.screenSpecialValue = 600;
        } else if (parentWidth > 1846 && parentWidth < 2200) {
            this.screenSpecialValue = 750;
        } else if (parentWidth > 2199) {
            this.screenSpecialValue = 900;
        } else if (parentWidth < 754) {
            this.screenSpecialValue = 280;
        }
        return this.screenSpecialValue;
    };

    MenuCutter.prototype.cutMenu = function () {
        let parentWidth = this.parent.offsetWidth;
        let count = this.list.children.length - 2;
        let childrenWidth = this.sumChildrenWidth();
        let screenSpecialValue = this.setScreenSpecialValue();

        if (screen.width > 767) {
            while (parentWidth <= (childrenWidth + screenSpecialValue)) {
                if (typeof(this.list.children[count]) === "object") {
                    this.list.children[count].classList.add('inside');
                    this.ul.appendChild(this.list.children[count]);
                    --count;
                    childrenWidth = this.sumChildrenWidth();
                } else {
                    break;
                }
            }
        }
    };

    MenuCutter.prototype.enlargeMenu = function () {
        let parentWidth = this.parent.offsetWidth;
        let childrenWidth = this.sumChildrenWidth();
        let screenSpecialValue = this.setScreenSpecialValue() + 100;
        let newCount = this.ul.children.length - 1;
        let count = this.list.children.length - 1;

        if (screen.width > 767) {
            while (parentWidth >= (childrenWidth + screenSpecialValue)) {
                this.ul.children[newCount].classList.remove('inside');
                this.list.insertBefore(this.ul.children[newCount], this.list.children[count]);
                --newCount;
                childrenWidth = this.sumChildrenWidth();
            }
        }
    };

    let parent = document.querySelector('.categories-nav'),
        list = document.querySelector('.menu-list'),
        instance = new MenuCutter(parent, list);

    instance.handleElementsAttributes();
    instance.cutMenu();


    ////////////////////////////////// MENU POSITIONING ////////////////////////////////////


    const moreBtn = document.querySelector('.more');
    const nestedListNav = document.querySelector('.nested-list-nav');
    const specialLi = document.querySelector('.special-li');

    if (screen.width > 767) {
        setPositionLeft(nestedListNav, specialLi);
    }

    function setPositionLeft(object, goalObject) {
        if (object !== null) {
            object.style.left = (goalObject.offsetLeft - 70) + "px";
        }
    }


    //////////////////////////////// RESIZE_WINDOW & MENU_SHOW ///////////////////////////////////////


    if (moreBtn !== null) {
        moreBtn.addEventListener("click", function () {
            nestedListNav.classList.toggle("hide");
        });
    }

    window.addEventListener('resize', function () {
        const specialLi = document.querySelector('.special-li');
        let specialLiExist = specialLi !== null ? true : false;

        if (!specialLiExist) {
            instance.handleElementsAttributes();
            specialLiExist = true;
        }

        if (screen.width > 767) {
            setPositionLeft(nestedListNav, specialLi);
            instance.cutMenu();
            instance.enlargeMenu();
        }

        if (!searchField.classList.contains('hide')) {
            searchField.classList.remove('show-search-field');
            setTimeout(function () {
                searchField.classList.add('hide')
            }, 700);
        }
    });

    /* SWIPER */

    let mySwiper = new Swiper('.slider-banner', {
        direction: 'horizontal',
        loop: true,
        navigation: {
            nextEl: '.button-banner-next',
            prevEl: '.button-banner-prev',
        }
    });

    let mySwiper1 = new Swiper('.slider-products', {
        direction: 'horizontal',
        loop: false,
        navigation: {
            nextEl: '.button-products-next',
            prevEl: '.button-products-prev',
        }
    });

    let mySwiper2 = new Swiper('.slider-products-lower', {
        direction: 'horizontal',
        loop: false,
        navigation: {
            nextEl: '.button-products-lower-next',
            prevEl: '.button-products-lower-prev',
        }
    });

    /* END_SWIPER */

    //////////////////////////////////// CLOSING MENU/SEARCH FIELD ON CLICK OUTSIDE & INSIDE ////////////////////////////////////////

    const mobileMenuIcon = document.querySelector('.mobile-menu-icon');
    const mobileMenuContainer = document.querySelector('.mobile-menu-container');
    const mobileMenuChoose = document.querySelector('.mobile-menu-choose');
    const mobileMenuChildren = mobileMenuChoose.querySelectorAll('a');

    window.addEventListener('click', function (e) {
        if (!mobileMenuContainer.classList.contains('hide-mobile-menu') && (e.target.className !== 'mobile-menu-icon')) {
            mobileMenuContainer.classList.add('hide-mobile-menu');
        }
    });

    if (nestedListNav !== null) {
        window.addEventListener('click', function (e) {
            if (!nestedListNav.classList.contains('hide') && (e.target.className !== 'more menu-uppercase')) {
                nestedListNav.classList.add('hide');
            }
        });
    }

    window.addEventListener('click', function (e) {
        if (!searchField.classList.contains('hide') && (e.target.className !== 'search-field show-search-field') && (e.target.className !== 'search-img')) {
            searchField.classList.remove('show-search-field');
            setTimeout(function () {
                searchField.classList.add('hide')
            }, 700);
        }
    });

    mobileMenuIcon.addEventListener('click', function () {
        mobileMenuContainer.classList.toggle('hide-mobile-menu');
    });

    ////////////////////////////////////// SEARCHING MECHANISM ////////////////////////////////////

    const searchImg = document.querySelector('.search-img');
    const searchField = document.querySelector('.search-field');
    const menuList = document.querySelector('.menu-list');

    function childrenWidth(parent) {
        let sum = 0;

        for (let i = 0, length = parent.children.length; i < length; ++i) {
            sum += parent.children[i].offsetWidth;
        }

        return sum;
    }

    searchImg.addEventListener('click', function () {

        let isDesktop = screen.width > 1024 ? true : false;
        childrenWidth(menuList);

        if (isDesktop && childrenWidth(menuList) > 500) {
            searchField.style.setProperty('--search-field-width', '11em');
        } else {
            searchField.style.setProperty('--search-field-width', '8em');
        }


        if (searchField.classList.contains('hide')) {
            searchField.classList.remove('hide');
            setTimeout(function () {
                searchField.classList.add('show-search-field')
            }, 100);
        } else {
            searchField.classList.remove('show-search-field');
            setTimeout(function () {
                searchField.classList.add('hide')
            }, 700);
        }

    });

    //////////////////////////////////// NO SCROLL ON MOBILE MENU ////////////////////////////////////////

    const fixedElement = document.querySelector('.mobile-menu-container');

    fixedElement.addEventListener('touchmove', function (e) {
        e.preventDefault();
    }, false);

});